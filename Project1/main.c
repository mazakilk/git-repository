#include <stdio.h>

int main(void)
{
    int x = 0;
    int y = 0;
    int z = 0;
    char rgb = '#';
    char c;
    printf("Type color in RGB format:\n");
    if (scanf(" r g b (%d , %d , %d %c", &x, &y, &z, &c) != 4 || c != ')' || (x < 0 || x > 255) || (y < 0 || y > 255) || (z < 0 || z > 255))
    {
        printf("Invalid input.\n");
        return 0;
    }
    else{    
    	printf("%c%02X%02X%02X\n", rgb, x, y, z);
    }
    return 0;
}
