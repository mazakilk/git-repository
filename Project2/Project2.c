#define EPS 1e-10
#include <stdio.h>
#include <math.h>
int main() {
    double num1;
    double num2;
    char c;
    char eq;
    printf("Type formula:\n");
    if(scanf("%lf %c %lf %c",&num1,&c,&num2,&eq) != 4 ||
       (c != '+' && c != '-' && c != '*' && c != '/') ||
       eq != '=')
    {
        printf("Invalid input.\n");
        return -1;
    }
    switch(c)
    {
        case '+':
            printf("%g\n",num1 + num2);
            break;
        case '-':
            printf("%g\n",num1 - num2);
            break;
        case '*':
            printf("%g\n",num1 * num2);
            break;
        case '/':
            if (num2 < EPS && num2 > 0)
            {
                double result = ceil(num1/num2);
                printf("%g\n", result);
                return 1;
                
            }
            if(num2 == 0)
            {
                printf("Invalid input.");
                return -1;
            }
            else
            {
                
                long result = num1/num2;
                printf("%ld\n", result);
                
                break;
            }
        default:
            break;
            
     }
     return 0;
}

    
                   

