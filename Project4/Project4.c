#include <stdio.h>
#include <stdlib.h>

int main(void) {
    
    int size;
    int width;
    
    printf("Enter chessboard size:\n");
    if(scanf("%d", &size)!=1 || size <= 0)
    {
        printf("Invalid input.\n");
        return -1;
    }
    printf("Enter field width:\n");
    if(scanf("%d", &width)!=1 || width <= 0)
    {
        printf("Invalid input.\n");
        return -1;
    }
    int* board[size*width+2];
    for (int i = 0; i < size*width+2; i++)
        board[i] = (int*)malloc((size*width+2) * sizeof(int));
    int count = 0;
    for (int i = 0; i < size*width+2; i++)
        for (int j = 0; j < size*width+2; j++)
            board[i][j] = ++count;
    board[0][0] = '+';
    
    board[0][size*width+1] = '+';
    board[size*width+1][0] = '+';
    board[size*width+1][size*width+1] = '+';
    
    for (int i = 1; i < size*width+1; i++) {
        for (int j = 1; j < size*width+1; j++)
        {
            board[i][j] = ' ';
        }
        
    }
    board[0][0] = '+';
    board[0][size*width+1] = '+';
    board[size*width+1][0] = '+';
    board[size*width+1][size*width+1] = '+';
    
    for (int i = 1; i < size*width+1; i++) {
        board[0][i] = '-';
        board[i][0] = '|';
        board[size*width+1][i] = '-';
        board[i][size*width+1] = '|';
    }
    for (int i = 1; i < size*width+1; i++) {
        for (int j = 1; j < size*width+1; j++) {
            if((i%(width)==0 && i%(width*2)!=0 && j%(width*2)==0) || (j%(width)==0 && j%(width*2)!=0 && i%(width*2)==0))
            {
                for(int k=0; k < width; k++)
                {
                    for (int m = 0; m < width; m++)
                    {
                        board[i][j] = 'X';
                        board[i-k][j] = 'X';
                        board[i-m][j] = 'X';
                        board[i][j-k] = 'X';
                        board[i-k][j-k] = 'X';
                        board[i-m][j-k] = 'X';
                        board[i][j-m] = 'X';
                        board[i-k][j-m] = 'X';
                        board[i-m][j-m] = 'X';
                    }
                
                }
            }
        }
    }
    for (int i = 0; i < size*width+2; i++) {
        for (int j = 0; j < size*width+2; j++) {
            printf("%c", board[i][j]);
            }
        printf("\n");
    }
        
    
    
    return 0;
    
}
    
     

